package com.gitlab.cromefire.detektgitlab

import com.gitlab.cromefire.detektgitlab.models.Issue
import com.gitlab.cromefire.detektgitlab.models.Location
import com.gitlab.cromefire.detektgitlab.models.Positions
import com.gitlab.cromefire.detektgitlab.models.Severity
import io.gitlab.arturbosch.detekt.api.Debt
import io.gitlab.arturbosch.detekt.api.Detektion
import io.gitlab.arturbosch.detekt.api.OutputReport
import io.gitlab.arturbosch.detekt.api.SeverityLevel
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.Json
import java.security.MessageDigest
import io.gitlab.arturbosch.detekt.api.Location as DetektLocation

class DetektGitlabReport : OutputReport() {
    override val ending: String = "json"
    private val json by lazy {
        Json {
            encodeDefaults = true
        }
    }

    override fun render(detektion: Detektion): String {
        val digest = MessageDigest.getInstance("MD5")
        val violations = detektion.findings.flatMap { entry ->
            val (ruleSetId, findings) = entry
            findings.map { finding ->
                val checkName = finding.issue.id
                val description = finding.messageOrDescription()
                val categories = mapRuleSet(ruleSetId)
                val location = mapLocation(finding.location)
                val otherLocations = finding.references.map { mapLocation(it.location) }
                val remediationPoints = finding.issue.debt.asMinutes() * remediationScalar
                val severity = mapSeverity(finding.severity)
                val fingerprint = bytesToHex(
                    digest.digest(
                        finding.signature.encodeToByteArray()
                    )
                )
                digest.reset()
                Issue(
                    checkName,
                    description,
                    categories,
                    location,
                    otherLocations,
                    remediationPoints,
                    severity,
                    fingerprint,
                )
            }
        }
        return json.encodeToString(ListSerializer(Issue.serializer()), violations)
    }

    private companion object {
        const val byteMask = 0xff
        const val hoursInDay = 24
        const val minutesInHour = 60
        const val remediationScalar = 10_000

        fun mapSeverity(level: SeverityLevel): Severity = when (level) {
            SeverityLevel.ERROR -> Severity.Major
            SeverityLevel.WARNING -> Severity.Minor
            SeverityLevel.INFO -> Severity.Info
        }

        fun mapRuleSet(ruleSetId: String): List<String> = when (ruleSetId) {
            "complexity" -> listOf("Complexity")
            "naming" -> listOf("Clarity")
            "performance" -> listOf("Performance")
            "potential-bugs" -> listOf("Bug Risk")
            "style" -> listOf("Style")
            else -> listOf()
        }

        fun mapLocation(dLocation: DetektLocation): Location {
            val path = dLocation.filePath.relativePath?.toString() ?: error(
                "Relative path not available. " +
                    "(see https://gitlab.com/cromefire_/detekt-gitlab-report#relative-path-not-available)"
            )
            return Location(
                path,
                Positions(
                    Positions.Position(dLocation.source.line, dLocation.source.column)
                )
            )
        }

        fun Debt.asMinutes(): Int {
            val totalHours = days * hoursInDay + hours
            return totalHours * minutesInHour + mins
        }

        fun bytesToHex(hash: ByteArray): String {
            val hexString = StringBuilder(2 * hash.size)
            for (i in hash.indices) {
                val hex = Integer.toHexString(byteMask and hash[i].toInt())
                if (hex.length == 1) {
                    hexString.append('0')
                }
                hexString.append(hex)
            }
            return hexString.toString()
        }
    }
}
