package com.gitlab.cromefire.detektgitlab.models

import kotlinx.serialization.Serializable

@Serializable
internal data class Positions(
    /**
     * The position at which the code quality violation occurred.
     */
    val begin: Position,
) {
    @Serializable
    data class Position(
        /**
         * Starts at 1.
         */
        val line: Int,
        /**
         * Starts at 1.
         */
        val column: Int,
    )
}
