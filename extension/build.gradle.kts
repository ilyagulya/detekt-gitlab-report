import io.gitlab.arturbosch.detekt.Detekt

plugins {
    kotlin("jvm")
    kotlin("plugin.serialization")
    id("io.gitlab.arturbosch.detekt")
    id("com.github.johnrengelman.shadow")
    `maven-publish`
}

val ciTag: String? = System.getenv("CI_COMMIT_TAG")

group = "com.gitlab.cromefire"
val snapshotVersion = "0.3.2"
version = if (!ciTag.isNullOrBlank() && ciTag.startsWith("v")) {
    ciTag.substring(1)
} else {
    "$snapshotVersion-SNAPSHOT"
}
base.archivesName.set("detekt-gitlab-report")

repositories {
    mavenCentral()
}

dependencies {
    implementation("io.gitlab.arturbosch.detekt:detekt-api:1.21.0")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json") {
        version {
            strictly("[1.4, 2.0[")
            prefer("1.4.1")
        }
    }

    testImplementation("io.gitlab.arturbosch.detekt:detekt-test:1.21.0")

    detektPlugins(project(project.path))
    detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:1.21.0")
}

tasks {
    shadowJar {
        dependencies {
            exclude {
                it.moduleGroup == "io.gitlab.arturbosch.detekt"
            }
        }

        archiveClassifier.set("plugin")
        isPreserveFileTimestamps = false
        isReproducibleFileOrder = true
    }

    withType<Detekt> {
        reports {
            custom {
                reportId = "DetektGitlabReport"
                // This tells detekt, where it should write the report to,
                // you have to specify this file in the gitlab pipeline config.
                outputLocation.set(file("$buildDir/reports/detekt/gitlab.json"))
            }
        }
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
    withSourcesJar()
}

detekt {
    buildUponDefaultConfig = true
    config = files("$rootDir/.config/detekt.yml")
    basePath = rootDir.toString()
}

// Fix for https://github.com/johnrengelman/shadow/issues/651
components.withType(AdhocComponentWithVariants::class.java).forEach { c ->
    c.withVariantsFromConfiguration(project.configurations.shadowRuntimeElements.get()) {
        skip()
    }
}

publishing {
    publications {
        register<MavenPublication>("maven") {
            artifactId = "detekt-gitlab-report"

            from(components["java"])

            pom {
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("https://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("cromefire_")
                        name.set("Cromefire_")
                        email.set("cromefire_@outlook.com")
                    }
                }
                scm {
                    connection.set("scm:git:git://gitlab.com/cromefire_/detekt-gitlab-report.git")
                    developerConnection.set("scm:git:ssh://git@gitlab.com/cromefire_/detekt-gitlab-report.git")
                    url.set("https://gitlab.com/cromefire_/detekt-gitlab-report")
                }
            }
        }
    }
    repositories {
        maven("https://gitlab.com/api/v4/projects/25796063/packages/maven") {
            name = "gitlab"

            credentials(HttpHeaderCredentials::class.java) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN") ?: ""
            }
            authentication {
                register("header", HttpHeaderAuthentication::class.java)
            }
        }
    }
}
