plugins {
    kotlin("jvm") version "1.7.20" apply false
    kotlin("plugin.serialization") version "1.7.20" apply false
    id("io.gitlab.arturbosch.detekt") version "1.21.0" apply false
    id("com.github.johnrengelman.shadow") version "7.1.2" apply false
}
